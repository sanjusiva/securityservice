package com.example.securityservice.enums;

public enum Roles {
	ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
